//
//  Maintenance.swift
//  S70PPC
//
//  Created by Home on 12.03.2019.
//  Copyright © 2019 Karpat. All rights reserved.
//

import Foundation

class Maintenance  {
    
    var otorotasyonKarti    :Dictionary<String, Any>
    var densityKarti        :Dictionary<String, Any>
    var tgtReferanceKarti   :Dictionary<String, Any>
    var etfKarti            :Dictionary<String, Any>
    var ttvKarti            :Dictionary<String, Any>
    
    
    
    init() {
        
        // otorotasyon kartı
        let path = Bundle.main.path(forResource: "otorotasyonRpm", ofType: "plist")
        
        let kartlarDegeri = NSDictionary(contentsOfFile: path!)!
        
        otorotasyonKarti = kartlarDegeri as! Dictionary<String, Any>
        
        // density kartı
        let pathDensity = Bundle.main.path(forResource: "densitynew", ofType: "plist")
        
        let kartlarDegeriDensity = NSDictionary(contentsOfFile: pathDensity!)!
        
        densityKarti = kartlarDegeriDensity as! Dictionary<String, Any>
        
        // tgt referance kartı
        let pathTgtReferance = Bundle.main.path(forResource: "tgtReferance", ofType: "plist")
        
        let kartlarDegeriTgtReferance = NSDictionary(contentsOfFile: pathTgtReferance!)!
        
        let root = kartlarDegeriTgtReferance as! Dictionary<String, Any>
        
        let gonderilecek = root["701c"]
        
        tgtReferanceKarti = gonderilecek as! Dictionary<String, Double>
        
        // etf kartı
        let pathEtf = Bundle.main.path(forResource: "etf", ofType: "plist")
        
        let kartlarDegeriEtf = NSDictionary(contentsOfFile: pathEtf!)!
        
        etfKarti = kartlarDegeriEtf as! Dictionary<String, Any>
        
        // ttv kartı
        let pathTtv = Bundle.main.path(forResource: "ttvTablo", ofType: "plist")
        
        let kartlarDegeriTtv = NSDictionary(contentsOfFile: pathTtv!)!
        
        ttvKarti = kartlarDegeriTtv as! Dictionary<String, Any>
        
    }
    
    // MARK: Helper funcs
    
   private func araCizgiOlustur (altDeger: Int, altDegerData: Dictionary<String, Any>, ustDeger: Int, ustDegerData: Dictionary<String, Any>, istenenDeger: Int) -> Dictionary<String, Any>
    {
        
        // eski versiyon çalışıyor.
        // yeni değişiklikler ekliyorum.
     
        var yeniDictAlt = altDegerData
        var yeniDictUst = ustDegerData
        
        let fark = altDegerData.count - ustDegerData.count
        
        if fark > 0 {
            // ust deger az, ekleme yapacaz.
            
            yeniDictUst = cizgiyeNoktaEkle(veriDictionarty: ustDegerData, eklenecekAdet: fark)
        }
        
        if fark < 0 {
            // alt degere ekleme yapacaz
            
            yeniDictAlt = cizgiyeNoktaEkle(veriDictionarty: altDegerData, eklenecekAdet: (0 - fark))
            
        }
        
        let altDegerDoubleArray = arrayKeylariDoubleArrayYapVeSırala(donustürülecekDictionary: yeniDictAlt)
        let ustDegerDoubleArray = arrayKeylariDoubleArrayYapVeSırala(donustürülecekDictionary: yeniDictUst)
        
        //  iki dictionary de hazır. bunlardan istenilen aralıkta yeni bir aralık değer oluşturulacak.
        
        var gonderilecekDict = [String: Double]()
        
        for i in 0 ..< altDegerDoubleArray.count {
            
            let altString = String(altDegerDoubleArray[i])
            let ustString = String(ustDegerDoubleArray[i])
            
            let altDegerKey = altDegerDoubleArray[i]
            let altDegerSonucu = yeniDictAlt[altString] as! Double
            
            let ustDegerKey = ustDegerDoubleArray[i]
            let ustDegerSonucu = yeniDictUst[ustString] as! Double
            
            
            
            let yeniNokta = noktaEnterpole(altCizgiDegeri: altDeger, altDegerKey: altDegerKey, altDegerSonucu: altDegerSonucu, ustCizgiDegeri: ustDeger, ustdegerKey: ustDegerKey, ustDegerSonucu: ustDegerSonucu, hedefCizgiDegeri: istenenDeger)
            
            let noktaKeyArray = [String](yeniNokta.keys)
            let noktaKey = noktaKeyArray.first
            
            gonderilecekDict[noktaKey!] = yeniNokta[noktaKey!]
            
        }
        
        return gonderilecekDict as Dictionary<String, Any>
        
    }
    
   private func cizgiyeNoktaEkle(veriDictionarty: Dictionary<String, Any>, eklenecekAdet: Int ) -> Dictionary<String, Any> {
        
        // yeni metod.
        
        var gonderilecekDict = veriDictionarty
        
        let hedefSayi = veriDictionarty.count + eklenecekAdet
        
        while gonderilecekDict.count < hedefSayi {
            
            let siraliArray = arrayKeylariDoubleArrayYapVeSırala(donustürülecekDictionary: gonderilecekDict as Dictionary<String, Any>)
            
            var aralikMesafesiDict = [Double]()
            
            for i in 0..<siraliArray.count - 1 {
                
                let altKey = siraliArray[i]
                let altDeger = gonderilecekDict["\(altKey)"] as! Double
                
                let ustKey = siraliArray[i + 1]
                let ustDeger = gonderilecekDict["\(altKey)"] as! Double
                
                let mesafe = noktaArasiMesafeOlc(altDegerKey: altKey, altDeger: altDeger, ustDegerKey: ustKey, ustDeger: ustDeger)
                
                aralikMesafesiDict.append(mesafe)
            }
            
            let maxAralik = aralikMesafesiDict.max()!
            let maxAralikIndex = aralikMesafesiDict.firstIndex(of: maxAralik)!
            
            // nokta ekle
            
            let yeniKey = (siraliArray[maxAralikIndex] + siraliArray[maxAralikIndex + 1]) / 2
            
            
            let altDeger = gonderilecekDict["\(siraliArray[maxAralikIndex] )"] as! Double
            let ustDeger = gonderilecekDict["\(siraliArray[maxAralikIndex + 1] )"] as! Double
            let yeniDeger = (altDeger + ustDeger) / 2
            
            gonderilecekDict["\(yeniKey)"] = yeniDeger
            
        }
        
        return gonderilecekDict
    }
    
   private func arrayKeylariDoubleArrayYapVeSırala (donustürülecekDictionary: Dictionary<String, Any>) -> Array<Double>
    { // keylar double olabilmeli, aksi halde sıralama çalışmaz
        
        let keyArrayString = [String](donustürülecekDictionary.keys)
        
        var returnArray = [Double]()
        
        for key in keyArrayString
        {
            returnArray.append(Double(key)!)
        }
        
        returnArray.sort(by: <)
        
        return returnArray
        
    }
    
   private func noktaArasiMesafeOlc(altDegerKey: Double, altDeger: Double, ustDegerKey: Double, ustDeger: Double) -> Double {
        
        let xMesafesi = ustDegerKey - altDegerKey
        let yMesafesi = ustDeger - altDeger
        
        return (xMesafesi * xMesafesi + yMesafesi * yMesafesi).squareRoot()
    }
    
   private func noktaEnterpole (altCizgiDegeri: Int, altDegerKey: Double, altDegerSonucu: Double,ustCizgiDegeri: Int, ustdegerKey: Double, ustDegerSonucu: Double,  hedefCizgiDegeri: Int) -> Dictionary<String, Double>
    {
        
        let aralik = ustCizgiDegeri - altCizgiDegeri
        
        if aralik < 0 {
            
            print("aralık negatif")
            
        }
        
        let fark = hedefCizgiDegeri - altCizgiDegeri
        
        let keyDegeri = ((ustdegerKey - altDegerKey) / Double(aralik)) * Double(fark) + altDegerKey
        
        let valueDegeri = ((ustDegerSonucu - altDegerSonucu) / Double(aralik)) * Double(fark) + altDegerSonucu
        
        return [String(keyDegeri):valueDegeri]
    }
    
   private func enYakinKeylarıBul (dictionaryGönder: Dictionary<String, Any> , istenenKey: String) -> (String, String)
    {
        
        // let degerNS : NSString = istenenKey
        
        let degerDouble = Double(istenenKey)!   // degerNS.doubleValue
        
        var siraliArray = arrayKeylariDoubleArrayYapVeSırala(donustürülecekDictionary: dictionaryGönder)
        
        var farkArrayi = [Double]()
        
        for i in 0 ..< siraliArray.count {
            
            var fark =   siraliArray[i] - degerDouble
            
            if fark < 0 {
                fark = 0 - fark
            }
            farkArrayi.append(fark)
        }
        
        var siraliFarkArray = farkArrayi
        
        siraliFarkArray =  siraliFarkArray.sorted(by: <)
        
        let ilkKeyIndex = farkArrayi.firstIndex(of: siraliFarkArray[0])
        
        let ilkKey = String(siraliArray[ilkKeyIndex!])
        
        // aynı farka sahip iki değer olursa hata oluyor. olmaması için değeri silip tekrar sıralıyorum.
        var birdegerSilinmisDict = dictionaryGönder
        
        birdegerSilinmisDict.removeValue(forKey: String(siraliArray[ilkKeyIndex!]))
        
        siraliArray = arrayKeylariDoubleArrayYapVeSırala(donustürülecekDictionary: birdegerSilinmisDict)
        
        farkArrayi.removeAll()
        
        for i in 0 ..< siraliArray.count {
            
            var fark =   siraliArray[i] - degerDouble
            
            if fark < 0 {
                fark = 0 - fark
            }
            farkArrayi.append(fark)
        }
        
        siraliFarkArray = farkArrayi
        
        siraliFarkArray =  siraliFarkArray.sorted(by: <)
        
        let ikinciKeyIndex = farkArrayi.firstIndex(of: siraliFarkArray[0])
        
        let ikinciKey = String(siraliArray[ikinciKeyIndex!])
        
        if ilkKey.doubleValue() > ikinciKey.doubleValue() {
            return (ilkKey, ikinciKey)
        } else {
            return (ikinciKey, ilkKey)
        }
        
    }
    
   private func enterpolasyon (altDeger:(Double), altDegerSonucu:(Double), ustDeger:(Double), ustDegerSonucu:(Double), hedefDeger:(Double)) -> Double
    {
        
        let aralik = ustDeger - altDeger
        
        let fark = hedefDeger - altDeger
        
        let sonucFarki = (ustDegerSonucu - altDegerSonucu) / aralik
        
        // sonucFarki = sonucFarki / aralik;
        
        let hedefSonuc = altDegerSonucu + ( fark * sonucFarki)
        
        return hedefSonuc
    }
    
     // MARK: Otorotasyon hesaplamaları
   private func otorotasyonAraCizgiOlustur(densityAltitude: Double, gw: Int) -> Dictionary<String, Double> {
        
        let fark = gw % 500
        
        if fark == 0 {
            // tam deger vardır, tablo değerini gönderebiliriz.
            
            let gonderilecek = otorotasyonKarti["\(gw)"]
            
            return gonderilecek as! Dictionary<String, Double>
        } else {
            // ara irtifa için çizgi üretilecek.
            
            let altGw = gw - fark
            
            let ustGw = altGw + 500
            
            let yeniDict = araCizgiOlustur(altDeger: altGw, altDegerData: otorotasyonKarti["\(altGw)"] as! Dictionary<String, Any>, ustDeger: ustGw, ustDegerData: otorotasyonKarti["\(ustGw)"] as! Dictionary<String, Any>, istenenDeger: gw)
            
            return yeniDict as! Dictionary<String, Double>
        }
        
    }
    
    func otorotasyonRpm(densityAltitude: Int, gw: Int) -> Double {
        
        let duzeltilmisAltitude = Double(densityAltitude) / 1000.0
        
        let a = otorotasyonAraCizgiOlustur(densityAltitude: duzeltilmisAltitude, gw: gw)
        
        let keylar = enYakinKeylarıBul(dictionaryGönder: a, istenenKey: "\(duzeltilmisAltitude)")
        
        let altDeger = Double(keylar.0)!
        
        let ustDeger = Double(keylar.1)!
        
        let sonuc = enterpolasyon(altDeger: altDeger, altDegerSonucu: a[keylar.0]!, ustDeger: ustDeger, ustDegerSonucu: a[keylar.1]!, hedefDeger: duzeltilmisAltitude)
        
        return sonuc
    }
    
    // MARK: Density altitude hesaplamaları
    
    private func densityAraCizgiOlustur(pressureAltitude: Int, fat: Int) ->Dictionary<String, Double> {
        
        let fark = pressureAltitude % 1000
        
        if fark == 0 {
            // tam deger vardır, tablo değerini gönderebiliriz.
            
            let gonderilecek = densityKarti["\(pressureAltitude)"]
            
            return gonderilecek as! Dictionary<String, Double>
        } else {
            // ara irtifa için çizgi üretilecek.
            
            let altIrtifa = pressureAltitude - fark
            
            let ustIrtifa = altIrtifa + 1000
            
            let yeniDict = araCizgiOlustur(altDeger: altIrtifa, altDegerData: densityKarti["\(altIrtifa)"] as! Dictionary<String, Any>, ustDeger: ustIrtifa, ustDegerData: densityKarti["\(ustIrtifa)"] as! Dictionary<String, Any>, istenenDeger: pressureAltitude)
            
            return yeniDict as! Dictionary<String, Double>
        }
        
    }
    
    func densityAltitude(pressureAltitude: Int, fat: Int) -> Double {
        
        let a = densityAraCizgiOlustur(pressureAltitude: pressureAltitude, fat: fat)
        
        let keylar = enYakinKeylarıBul(dictionaryGönder: a, istenenKey: "\(fat)")
        
        let altDeger = Double(keylar.0)!
        
        let ustDeger = Double(keylar.1)!
        
        let sonuc = enterpolasyon(altDeger: altDeger, altDegerSonucu: a[keylar.0]!, ustDeger: ustDeger, ustDegerSonucu: a[keylar.1]!, hedefDeger: Double(fat))
        
        return sonuc
    }
    
    // MARK: Base tgt hesaplamaları
    
    let irtifalarTTV = ["0" ,"500" ,"1000" ,"1500" ,"2000" ,"2500" ,"3000" ,"3500" ,"4000" ,"4500" ,"5000" ,"5500" ,"6000" ,"6500" ,"7000" ,"7500" ,"8000","8500" ,"9000" ,"9500" ,"10000"]
    
    let sicakliklar = ["55", "50" ,"45" ,"39" ,"37" ,"35" ,"33" , "31", "29", "27", "25", "23", "21", "19", "17", "15", "13", "11", "9", "7", "5", "3", "1", "-1", "-3", "-5", "-7", "-9", "-11", "-13", "-15", "-17", "-19", "-21", "-23", "-25", "-27", "-29", "-31", "-33", "-35", "-37", "-39", "-45", "-50", "-55"]
    let irtifalar = ["-1000", "-500" ,"0" ,"500" ,"1000" ,"1500" ,"2000" ,"2500" ,"3000" ,"3500" ,"4000" ,"4500" ,"5000" ,"5500" ,"6000"
        ,"7000" ,"8000" ,"9000" ,"10000"]
    
    // tablo değerleri
    
    let tgtEksi1000 = [736, 721,706, 690, 684, 679, 673, 668, 662, 657, 651, 645, 639, 634, 628, 623, 617, 612, 606, 600, 595, 589, 584, 577, 571, 566, 560, 554, 549, 543, 538, 532, 526, 521, 515, 510, 504, 498, 492, 487, 482, 476, 470, 453, 439, 425]
    
    let tgtEksi500 = [740, 724, 709, 692, 687, 681, 676, 670, 665, 659, 654, 648, 642, 636, 631, 625, 620, 614, 609, 603, 598, 592, 586, 579, 574, 568, 563, 557, 552, 546, 540, 535, 529, 523, 518, 512, 507, 501, 495, 490, 484, 478, 473, 456, 441, 427]
    
    let tgt0 = [744,728,713,696,690,685,679,674,668,662,657,651,645,640,634,629,623,618,612,607,601,595,590,583,577,
    572,566,560,555,549,544,538,532,527,521,515,510,504,498,493,487,481,476,459,444,430]
    
    let tgt500 = [748,733,717,700,694,689,683,677,671,666,660,655,649,643,638,632,626,621,616,610,605,599,593,586,581,
    575,569,564,558,553,547,541,536,530,524,519,513,507,501,496,490,484,479,462,447,433]
    
    let tgt1000 = [753,737,721,704,698,692,687,681,675,670,664,658,652,647,641,635,630,624,619,613,608,602,597,590,
    584,578,573,567,561,556,550,544,539,533,527,522,516,510,504,499,493,487,482,465,450,436]
    
    let tgt1500 = [758,742,725,707,702,695,690,685,679,673,667,662,656,650,644,638,633,627,622,616,611,605,600,593,
    587,581,576,570,564,559,553,547,542,536,530,524,519,513,507,501,496,490,484,467,453,438]
    
    let tgt2000 = [763,747,731,712,706,700,694,689,683,677,671,666,660,654,648,642,636,631,625,620,614,609,603,596,
    591,585,579,574,568,562,556,551,545,539,534,528,522,516,511,505,499,493,487,470,456,441]
    
    let tgt2500 = [769,752,736,716,710,704,698,692,687,681,675,669,664,658,652,646,640,634,629,623,618,612,607,600,
    594,589,583,577,571,566,560,555,549,543,537,531,526,520,514,508,502,497,491,473,459,444]
    
    let tgt3000 = [775,758,741,721,714,709,703,697,691,685,680,674,668,662,656,650,644,638,633,627,621,616,610,603,
    598,592,587,581,575,569,564,558,552,548,540,535,529,523,517,511,506,500,494,476,462,447]
    
    let tgt3500 = [781,764,747,725,720,713,707,702,696,690,684,678,672,667,661,655,649,643,637,631,625,620,614,607,
    602,596,590,585,579,573,567,562,556,550,544,538,532,525,521,515,509,503,497,479,465,450]
    
    let tgt4000 = [787,770,753,732,725,719,712,707,701,695,689,683,677,671,665,659,654,647,641,635,630,624,618,611,
    606,600,594,589,583,577,571,565,559,554,548,542,536,530,524,518,512,506,500,483,468,454]
    
    let tgt4500 = [792,776,759,738,731,724,717,711,705,700,694,688,682,676,670,664,658,652,646,640,634,629,622,615,
    609,604,598,592,586,581,575,569,563,557,551,545,539,534,528,522,516,510,504,486,471,457]
    
    let tgt5000 = [796,782,764,744,737,730,723,716,710,704,698,692,686,680,674,668,662,656,650,644,637,631,626,618,
    613,607,601,596,590,584,578,572,566,560,555,549,543,537,531,525,519,513,507,489,475,460]
    
    let tgt5500 = [801,788,771,750,743,736,729,722,715,709,703,697,691,685,679,673,666,660,654,648,642,636,629,622,
    617,611,605,599,594,588,582,576,570,564,558,552,547,541,535,529,523,517,511,493,478,464]
    
    let tgt6000 = [805,792,776,755,748,741,735,727,720,714,707,701,695,689,683,677,671,665,659,652,646,640,634,625,
    620,615,609,603,597,591,586,580,574,568,562,556,550,545,539,533,527,521,515,497,482,467]
    
    let tgt7000 = [814,802,788,768,761,754,746,739,732,725,718,711,705,699,693,686,680,674,668,662,656,649,643,635,
    629,623,617,611,606,600,594,588,582,577,571,565,559,553,547,541,539,529,523,505,490,475]
    
    let tgt8000 = [825,811,798,780,773,766,759,752,745,737,730,723,715,709,703,697,690,684,678,671,665,659,653,645,
    639,632,625,620,614,608,602,596,591,585,579,573,567,561,554,548,542,536,530,512,497,481]
    
    let tgt9000 = [835,822,808,792,787,779,772,765,758,750,743,736,728,721,714,708,701,695,689,682,676,669,663,655,
    649,642,636,630,623,617,611,605,599,593,587,581,575,569,563,556,550,544,538,519,504,489]
    
    let tgt10000 = [846,832,818,802,796,791,786,779,771,764,756,749,742,734,727,719,712,706,699,693,687,680,674,666,
    659,652,646,639,633,627,620,614,608,602,596,589,583,577,571,565,558,552,546,527,512,496]

   lazy var tgtIrtifalar = [tgtEksi1000, tgtEksi500, tgt0, tgt500, tgt1000, tgt1500, tgt2000, tgt2500, tgt3000, tgt3500, tgt4000, tgt4500, tgt5000, tgt5500, tgt6000, tgt7000, tgt8000, tgt9000, tgt10000 ]
    
    func baselineTgt(altitude: Int, fat: Int) -> Int {
        
        let altitudeIndex = irtifalar.firstIndex(of: "\(altitude)")!
        
        let fatIndex = sicakliklar.firstIndex(of: "\(fat)")!
        
        let baseTgt = tgtIrtifalar[altitudeIndex][fatIndex]
        
        return baseTgt
    }
    
    func tgtReferanceHesapla(fat: Int) -> Int {
        
        let keylar = enYakinKeylarıBul(dictionaryGönder: tgtReferanceKarti, istenenKey: "\(fat)")
        
        let altDeger = Double(keylar.0)!
        
        let ustDeger = Double(keylar.1)!
        
        let sonuc = enterpolasyon(altDeger: altDeger, altDegerSonucu: tgtReferanceKarti[keylar.0] as! (Double), ustDeger: ustDeger, ustDegerSonucu: tgtReferanceKarti[keylar.1] as! (Double), hedefDeger: Double(fat))
        
        return Int(sonuc)
    }
    
    // MARK: etf hesaplamaları
    private func etfAraCizgiOlustur(str: Double, fat: Int) -> Dictionary<String, Double> {
        
        let fark = fat % 5
        
        if fark == 0 {
            // tam deger vardır, tablo değerini gönderebiliriz.
            
            let gonderilecek = etfKarti["\(fat)"]
            
            return gonderilecek as! Dictionary<String, Double>
        } else {
            // ara irtifa için çizgi üretilecek.
            
            let altFat = fat - fark
            
            let ustFat = altFat + 5
            
            let yeniDict = araCizgiOlustur(altDeger: altFat, altDegerData: etfKarti["\(altFat)"] as! Dictionary<String, Any>, ustDeger: ustFat, ustDegerData: etfKarti["\(ustFat)"] as! Dictionary<String, Any>, istenenDeger: fat)
            
            return yeniDict as! Dictionary<String, Double>
        }
        
    }
    
    func etf(str: Double, fat: Int) -> Double {
        
        var duzeltilmisFat = fat
        
        if fat < -5 {
           duzeltilmisFat = -5
        }
        
        if fat >= 35 {
            return str
        }
        
        let a = etfAraCizgiOlustur(str: str, fat: duzeltilmisFat)
        
        let keylar = enYakinKeylarıBul(dictionaryGönder: a, istenenKey: "\(str)")
        
        let altDeger = Double(keylar.0)!
        
        let ustDeger = Double(keylar.1)!
        
        let sonuc = enterpolasyon(altDeger: altDeger, altDegerSonucu: a[keylar.0]!, ustDeger: ustDeger, ustDegerSonucu: a[keylar.1]!, hedefDeger: str)
        
        return sonuc
    }
    
    // MARK: ttv hesaplamaları
//    private func ttvAraCizgiOlustur(altitude: Int, fat: Int) -> Dictionary<String, Double> {
//
//        let fark = altitude % 2000
//
//        if fark == 0 {
//            // tam deger vardır, tablo değerini gönderebiliriz.
//
//            let gonderilecek = ttvKarti["\(altitude)"]
//
//            return gonderilecek as! Dictionary<String, Double>
//        } else {
//            // ara irtifa için çizgi üretilecek.
//
//            let altAltitude = altitude - fark
//
//            let ustAltitude = altAltitude + 2000
//
//            let yeniDict = araCizgiOlustur(altDeger: altAltitude, altDegerData: ttvKarti["\(altAltitude)"] as! Dictionary<String, Any>, ustDeger: ustAltitude, ustDegerData: ttvKarti["\(ustAltitude)"] as! Dictionary<String, Any>, istenenDeger: altitude)
//
//            return yeniDict as! Dictionary<String, Double>
//        }
//
//    }
    
    func ttv(altitude: Int, fat: Int) -> Double {
        
        let irtifaSecili = ttvKarti["\(altitude)"] as! Dictionary<String, Double>
       
        let sonuc = irtifaSecili["\(fat).0"]
        
        return sonuc!
        
//        let a = ttvAraCizgiOlustur(altitude: altitude, fat: fat)
//
//        let keylar = enYakinKeylarıBul(dictionaryGönder: a, istenenKey: "\(fat)")
//
//        let altDeger = Double(keylar.0)!
//
//        let ustDeger = Double(keylar.1)!
//
//        let sonuc = enterpolasyon(altDeger: altDeger, altDegerSonucu: a[keylar.0]!, ustDeger: ustDeger, ustDegerSonucu: a[keylar.1]!, hedefDeger: Double(fat))
//
//        return sonuc
    }
    
    func yuvarla (deger:Double) -> Int
    {
        
        let fark = deger.truncatingRemainder(dividingBy: 1);
        
        var degerInt = Int(deger)
        
        if (fark >= 0.5) {
            degerInt = degerInt + 1;
        }
        return degerInt;
        
    }
    
    
    func hitBaseLine(deger1: Int, deger2: Int, deger3: Int, irtifa: Int, sicaklik: Int) -> (average: Int, table: Int, tgtMargin: String) {
        
        let avegare = (Double(deger1) + Double(deger2) + Double(deger3)) / 3
        
        let avegareYuvarlanmis = yuvarla(deger: avegare)
        
        let tablo = baselineTgt(altitude: irtifa, fat: sicaklik)
        
        let fark = avegareYuvarlanmis - tablo
        
        let margin = "\(fark + 20) / \(fark - 20)"
        
        return (avegareYuvarlanmis, tablo, margin)
    }
    
    func deneme() -> Bool{
        
       // _ = hitBaseLine(deger1: 666, deger2: 660, deger3: 663, irtifa: 6000, sicaklik: 5)
        
         _ = maxPowerCheck(irtifa: 6000, sicaklik: -16, tq: 97, tgt: 820)
        return true
    }
    
    func maxPowerCheck(irtifa: Int, sicaklik: Int, tq: Int, tgt: Int) -> (ttv: Double, stv: Double, etf: Double) {
        
        let tqDouble = Double(tq)
        
        var tqAdj = tqDouble
        
        if sicaklik <= -14 {
            
        let tgtRefValue = tgtReferanceHesapla(fat: sicaklik)
    
        tqAdj = tqDouble + 0.2 * Double(tgtRefValue) - 0.2 * Double(tgt)
            
        }
        
        let ttvYeni = ttv(altitude: irtifa, fat: sicaklik)
        
        let str = tqAdj / ttvYeni
        
        var etfYeni = etf(str: str, fat: sicaklik)
        
        if str >= 1.0 {
            etfYeni = 1.0
        }
        
       
        return(ttvYeni, str, etfYeni)
    }
    
}
