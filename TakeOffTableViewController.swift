//
//  TakeOffTableViewController.swift
//  S70PPC
//
//  Created by Mustafa Karpat on 4.02.2017.
//  Copyright © 2017 Karpat. All rights reserved.
//

import UIKit
import JGProgressHUD



class TakeOffTableViewController: UITableViewController, ButtonCellDelegate, UITextFieldDelegate, fatButtonDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    enum RowCinsi {
        
        case TextCell
        case TextBoxWithButtonCell
        case TwoLabelCell
        case DoubleTextBoxCell
        case SwicthCell
        case Buttoncell
        
    }
    
    let facade = Facade.sharedInstance
    
    let hud = MyHud.init(style: .dark)
   
    
    let rowCinsi: [RowCinsi] = [ .TextCell, .TextBoxWithButtonCell, .TwoLabelCell, .DoubleTextBoxCell, .SwicthCell, .SwicthCell, .Buttoncell]
    
    let rowTitle = ["Pressure Altitude", "Fat", "Gross Weight", "etf satırı", "Anti-Ice", "Heater" ] 
    
    let placeHolders = ["Feet", " ℃",]
    
    let responderInfo = ["Pressure Altitude": 0,
                         "Fat":1,
                         "Gross Weight":2,
                         "etf1":3,
                         "etf2":3]
    
    var gwSatiriYuksekligi: CGFloat = 41
    
    override func viewDidLoad() {
         super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if facade.takeOffConditions.maxGwOnerilebilir() {
                  gwSatiriYuksekligi = 70
               }
        
        if let path = indexpathForInfo(info: "Gross Weight") {
        
        tableView.reloadRows(at: [path], with: .fade)
        }
        
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return rowCinsi.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        let cins = rowCinsi[indexPath.row]
        
        switch cins {
            
        case .TextCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextBoxCell", for: indexPath) as! TextCell
            
            cell.title.text = rowTitle[indexPath.row]
            cell.textField.info = rowTitle[indexPath.row]
            cell.textField.placeholder = placeHolders[indexPath.row]
            cell.selectionStyle = .none
            
            if cell.textField.info == "Pressure Altitude" && facade.takeOffConditions.altitude != nil {
                // katyıtlı değer vardır. 
                
                cell.textField.text = "\(facade.takeOffConditions.altitude!)"
                
            }
            
            return cell
        
        case .TextBoxWithButtonCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextBoxWithButtonCell", for: indexPath) as! TextBoxWithButtonCell
            
            cell.title.text = rowTitle[indexPath.row]
            cell.textField.info = rowTitle[indexPath.row]
            cell.textField.placeholder = placeHolders[indexPath.row]
            cell.selectionStyle = .none
            
            cell.delegate = self
            
            if facade.takeOffConditions.fat != nil {
                // deger vardır. 
                
                cell.textField.text = "\(facade.takeOffConditions.fat!)"
            }
            
            return cell
            
        case .TwoLabelCell:
            // gross Weight cell i.
            let cell = tableView.dequeueReusableCell(withIdentifier: "TwoLabelCell", for: indexPath) as! TwoLabelCell
            
            cell.title.text = rowTitle[indexPath.row]
            
            cell.result.text = "\(facade.takeOffGrossWeight.totalGW) lb"
            
            if facade.takeOffConditions.gwOnerisi != nil {
                cell.gwTavsiyeLabel.text = facade.takeOffConditions.gwOnerisi!
            }
            
            return cell
            
        case .DoubleTextBoxCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoubleTextBoxCell", for: indexPath) as! DoubleTextBoxCell
            
            if facade.aircraft.etf1 != nil {
            
                cell.etf1TextField.text = String(format: "%.2f", facade.aircraft.etf1!)
            
            }
            
            if facade.aircraft.etf2 != nil {
           
            cell.etf2TextField.text = String(format: "%.2f", facade.aircraft.etf2!)
                
            }
            
             cell.etf1TextField.info = "etf1"
             cell.etf2TextField.info = "etf2"
            cell.selectionStyle = .none
            
            return cell
        case . SwicthCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchCell", for: indexPath) as! SwitchCell
            
            cell.title.text = rowTitle[indexPath.row]
            cell.anahtar.info = rowTitle[indexPath.row]
            
            cell.anahtar.addTarget(self, action: #selector(anahtarDegisti), for: .valueChanged)
            
            switch cell.anahtar.info! {
            case "Anti-Ice":
                if facade.takeOffConditions.antiIce {
                    cell.anahtar.setOn(true, animated: true)
                } else {
                    cell.anahtar.setOn(false, animated: true)
                }
            case "Heater":
                if facade.takeOffConditions.heater {
                    cell.anahtar.setOn(true, animated: true)
                } else {
                    cell.anahtar.setOn(false, animated: true)
                }
                
            default:
                break
            }
            
            cell.selectionStyle = .none
            
            return cell
        case .Buttoncell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.delegate = self
            cell.selectionStyle = .none
            
            return cell
            
        }

    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.row == 3 {
            return 78
        } else  if indexPath.row == 2{
            return gwSatiriYuksekligi
        }else {
            return 44
        }


    }
    
    func gwOnerisiGoster() {
        
        let sonuc = facade.calculator.maxGWonerisiYap(conditions: facade.takeOffConditions)
        facade.takeOffConditions.gwOnerisi = "\(sonuc.ige)lb / \(sonuc.oge)lb "
        gwSatiriYuksekligi = 70
        
        if let path = indexpathForInfo(info: "Gross Weight") {
              tableView.reloadRows(at: [path], with: .middle)
              }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 2 {
            // gw satırı ise ?
            
            performSegue(withIdentifier: "takeoffGW", sender: nil)
            
        }
        
        
    }
    
    func butonaBasildi(_ sender: UIButton) {
        
      
        self.view.endEditing(true)
        
      let valid =  facade.takeOffConditions.validate()
        
        if valid.kabulEdildi {
            
           //  HUD.show(.progress)
            
            hud.showPieHUD(in: self.view)
            
            DispatchQueue.global(qos: .userInitiated).async { [weak weakSelf = self] in
                
                let observation = self.facade.calculator.observe(\Calculator.progress) { (model, change) in
                    
                    DispatchQueue.main.async {
                        self.hud.incrementHUD(progress: model.progress)
                        
                    }
                    
                }
                
                let sonuc = weakSelf?.facade.calculateTakeoff(conditions: (weakSelf?.facade.takeOffConditions)!)
                
                if (sonuc?.success)! {
                    // hesaplama başarılı olmuş.
                    
                    
//
//                    DispatchQueue.main.async {
//
//                        HUD.flash(.success, delay: 0.2) { [weak weakSelf = self] finished in
//
//                            // result sayfasına geçiş burda olacak.
//
//
//                            weakSelf?.performSegue(withIdentifier: "takeoffResults", sender: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                        UIView.animate(withDuration: 0.1, animations: {
                            self.hud.textLabel.text = "Success"
                            self.hud.detailTextLabel.text = nil
                            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        })
                        
                        self.hud.dismiss(afterDelay: 1.0)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400)) {
                            
                            weakSelf?.performSegue(withIdentifier: "takeoffResults", sender: nil)
                    
                    
                        }
                        
                    }
                    
                } else {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(20)) {
                        
                        UIView.animate(withDuration: 0.1, animations: {
                            self.hud.textLabel.text = "Error"
                            self.hud.detailTextLabel.text = nil
                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        })
                        
                        self.hud.dismiss(afterDelay:1.0)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(900)) {
                            
                            if sonuc?.Baslik != nil && sonuc?.mesaj != nil {
                                
                                weakSelf?.showAlert(title: (sonuc?.Baslik)!, message: (sonuc?.mesaj)!, info: nil)
                                
                            }
                            
                        }
                        
                    // hesaplamada hata var. gerekli mesajı göstermek lazım
//                    DispatchQueue.main.async {
//
//                        HUD.flash(.error, delay: 0.5) { [weak weakSelf = self ] finished in
//
//                            if sonuc?.Baslik != nil && sonuc?.mesaj != nil {
//
//                            weakSelf?.showAlert(title: (sonuc?.Baslik)!, message: (sonuc?.mesaj)!, info: nil)
//
//                            }
//                        }
//
                    
                        
                    }
                }
                
                observation.invalidate()
                
            }
            
            
            
        } else {
            
            showAlert(title: valid.baslik, message: valid.mesaj, info: nil)
            
        }
        

    }

    @objc func anahtarDegisti(_ sender: Any ) {
        
        if let asd = sender as? MySwitch {
            switch asd.info! {
            case "Anti-Ice":
                
                if asd.isOn {
                    facade.takeOffConditions.antiIce = true
                } else {
                    facade.takeOffConditions.antiIce = false
                }
                
            case "Heater":
                
                if asd.isOn {
                    facade.takeOffConditions.heater = true
                } else {
                    facade.takeOffConditions.heater = false
                }
                
            default:
                break
            }
        }
        
        if facade.takeOffConditions.maxGwOnerilebilir() {
            gwOnerisiGoster()
        }
        
    }
    
    func negatifButonaBasildi(_ sender: UITextField) {
        
        // butona basıldığında editlenmiş gibi işlem görmesi için .
        textFieldDidEndEditing(sender )
        
        
    }
    
    // MARK : pickerview
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    let pickerValues = ["0.85", "0.86", "0.87","0.88","0.89","0.90","0.91","0.92","0.93","0.94","0.95","0.96","0.97","0.98","0.99","1.00"]
    
    var selectedTexfield = UITextField()
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedTexfield.text = pickerValues[row]
        
        
    }
    
    // MARK : textfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        selectedTexfield = textField
        
        if let myfield = textField as? MyTextField {
            
            if myfield.info! == "etf1" || myfield.info! == "etf2"{
                
                let picker = UIPickerView()
                
                picker.delegate = self
                
                if textField.text != "" {
                    
                    let row = pickerValues.firstIndex(of: textField.text!)
                    
                    picker.selectRow(row!, inComponent: 0, animated: true)
                    
                }else {
                    picker.selectRow(15, inComponent: 0, animated: true)
                    textField.text = pickerValues[15]
                }

                
                textField.inputView = picker
            }
        }
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text != "" {
            // boş değer girilmemişse
            if let myifeld = textField as? MyTextField {
                
                
                
                
                switch myifeld.info! {
                case "Pressure Altitude":
                    if let IntValue = Int( myifeld.text!) {
                        
                    let value = facade.takeOffConditions.setAltitude(value: IntValue)
                    
                    if value.kabul {
                        
                        // deger aktarıldı. aktarma sonrası update yapmak istiyorsan buraya ekle. 
                        
                        
                    } else {
                        
                        showAlert(title: value.alertBasligi, message: value.alertMetni, info: myifeld.info!)
                        
                    }
                    }

                case "Fat":
                    if let IntValue = Int( myifeld.text!) {
                    
                    let value = facade.takeOffConditions.setFat(value: IntValue)
                    
                    if value.kabul {
                        // deger aktarıldı. aktarma sonrası update yapmak istiyorsan buraya ekle.
                    } else {
                        showAlert(title: value.alertBasligi, message: value.alertMetni, info: myifeld.info!)
                    }
                    }
                case "etf1":
                    
                    let doubleValue = Double(myifeld.text!)
                    
                    let attempt = facade.aircraft.setEtf1(deger: doubleValue!)
                    
                    if !attempt.kabul {
                        showAlert(title: attempt.baslik, message: attempt.mesage, info: "etf1")
                    }
                    
                case "etf2":
                    
                    let doubleValue = Double(myifeld.text!)
                        
                     let attempt = facade.aircraft.setEtf2(deger: doubleValue!)
                    
                    if !attempt.kabul {
                        showAlert(title: attempt.baslik, message: attempt.mesage, info: "etf2")
                    }

                
                default:
                    break
                }
            
                
                
                }
            
        } else {
            // eski değeri geri yazdır. 
            
            if let myfield = textField as? MyTextField {
                
                if let path = indexpathForInfo(info: myfield.info!) {
                
                tableView.reloadRows(at: [path], with: .fade)
                    
                }
                
            }
            
            
            
        }
        
        if facade.takeOffConditions.maxGwOnerilebilir() {
            gwOnerisiGoster()
        } else {
            
        }
        
    }
    
    func showAlert(title: String, message : String, info: String? ) {
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK",
                                     style: UIAlertAction.Style.default,
                                     handler: { [weak weakSelf = self] (alert: UIAlertAction!) in
                                        
                                        if info != nil {
                                        weakSelf?.alertOkIleKapatıldı(info: info!)
                                        }
        })
        
        alert.addAction(okAction)
        
        
        present(alert, animated: true, completion: nil)
    }

    func alertOkIleKapatıldı(info: String ) {
        
        if let path = indexpathForInfo(info: info) {
            
            switch info  {
            case "Pressure Altitude":
             
               let cell = tableView.cellForRow(at: path ) as? TextCell
               
               cell?.textField.becomeFirstResponder()
                
            case "Fat":
                
                let cell = tableView.cellForRow(at: path ) as? TextBoxWithButtonCell
                
                cell?.textField.becomeFirstResponder()
                
            case "etf1":
                
                let cell = tableView.cellForRow(at: path ) as? DoubleTextBoxCell
                
                cell?.etf1TextField.becomeFirstResponder()
                
            case "etf2":
                
                let cell = tableView.cellForRow(at: path ) as? DoubleTextBoxCell
                
                cell?.etf2TextField.becomeFirstResponder()
                
            default:
                break
            }
            
            
        
        
        
    }
        
}

    func indexpathForInfo(info: String) -> IndexPath? {
        
        if let rowa = responderInfo[info] {
            
            let path = IndexPath(row: rowa, section: 0)
            return path
        }
        return nil
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let vc = segue.destination as? GWTableViewController {
            
            vc.jenericGW = facade.takeOffGrossWeight
            
        }
        
        if let vc = segue.destination as? TakeOffResultsTableViewController {
            
            vc.results = facade.takeOffResults
        }
        
        
    }
    

}











