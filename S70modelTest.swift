//
//  S70modelTest.swift
//  S70PPCTests
//
//  Created by Home on 18.09.2017.
//  Copyright © 2017 Karpat. All rights reserved.
//

import XCTest

@testable import Pods_S70PPC

class S70modelTest: XCTestCase {
    
    let model = S70PPC.init()
    
    // extension testleri -->
    
    func testTorqueRatio() {
        // torque Ratio
        XCTAssertEqual(model.torqueFactor(etf1: 0.9, fat1: 25), 0.922, accuracy: 0.001)
        XCTAssertEqual(model.torqueFactor(etf1: 0.95, fat1: 40), 0.95, accuracy: 0.001)
        XCTAssertEqual(model.torqueFactor(etf1: 0.97, fat1: -10), 0.992, accuracy: 0.001)
        XCTAssertEqual(model.torqueFactor(etf1: 0.88, fat1: 12), 0.938, accuracy: 0.001)
        XCTAssertEqual(model.torqueFactor(etf1: 0.91, fat1: 32), 0.915, accuracy: 0.001)
        XCTAssertEqual(model.torqueFactor(etf1: 1.0, fat1: 18), 1.0, accuracy: 0.001)
    }
    
    func testEngineHignAmbientTemperatureLimit() {
        
    
        // engine high ambient limit
        
        XCTAssertTrue(model.doesEngineHighAmbientLimitReached(altitude: 12000, fat: 30))
        XCTAssertTrue(model.doesEngineHighAmbientLimitReached(altitude: 4000, fat: 47))
        XCTAssertTrue(model.doesEngineHighAmbientLimitReached(altitude: 7000, fat: 38))
        XCTAssertTrue(model.doesEngineHighAmbientLimitReached(altitude: 0, fat: 56))
        
        XCTAssertFalse(model.doesEngineHighAmbientLimitReached(altitude: 1000, fat: 50))
        XCTAssertFalse(model.doesEngineHighAmbientLimitReached(altitude: 4000, fat: 42))
    }
    
    func testMaxTq() {
        // maxTQ 2.5 min
        
        XCTAssertEqual(model.maxTQ2min(altitude: 0, fat: 10), 135.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 2000, fat: 10), 126.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 4000, fat: 10), 117.1, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 6000, fat: 10), 108.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 8000, fat: 10), 100.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 10000, fat: 10), 92.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 12000, fat: 10), 85.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 14000, fat: 10), 78.3, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 16000, fat: 10), 72.02, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 18000, fat: 10), 66.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 20000, fat: 10), 60.2, accuracy: 0.5)
        
        XCTAssertEqual(model.maxTQ2min(altitude: 1000, fat: 31), 124.4, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 2500, fat: 22), 120.3, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 5200, fat: 15), 110.5, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 6500, fat: 10), 106.6, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 8100, fat: 1), 102.5, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 11000, fat: -4), 92.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 12500, fat: -8), 86.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 15500, fat: -10), 77.1, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 16900, fat: -15), 72.02, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 19000, fat: -20), 65.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ2min(altitude: 19300, fat: -24), 63.5, accuracy: 0.5)
        
        // maxTQ 10 min
        
        XCTAssertEqual(model.maxTQ10min(altitude: 0, fat: 10), 132.5, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 2000, fat: 10), 123.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 4000, fat: 10), 114.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 6000, fat: 10), 105.9, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 8000, fat: 10), 97.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 10000, fat: 10), 90.1, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 12000, fat: 10), 83.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 14000, fat: 10), 76.4, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 16000, fat: 10), 70.1, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 18000, fat: 10), 64.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 20000, fat: 10), 58.6, accuracy: 0.5)
        
        XCTAssertEqual(model.maxTQ10min(altitude: 1000, fat: 31), 119.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 2500, fat: 22), 116.9, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 5200, fat: 15), 107.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 6500, fat: 10), 103.9, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 8100, fat: 1), 100.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 11000, fat: -4), 90.5, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 12500, fat: -8), 86.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 15500, fat: -10), 76.5, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 16900, fat: -15), 72.02, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 19000, fat: -20), 65.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ10min(altitude: 19300, fat: -24), 63.5, accuracy: 0.5)
        
        // maxTQ 30 min
        
        XCTAssertEqual(model.maxTQ30min(altitude: 0, fat: 10), 126.6, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 2000, fat: 10), 117.4, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 4000, fat: 10), 109.3, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 6000, fat: 10), 101.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 8000, fat: 10), 93.4, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 10000, fat: 10), 85.9, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 12000, fat: 10), 78.8, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 14000, fat: 10), 72.1, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 16000, fat: 10), 66.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 18000, fat: 10), 60.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 20000, fat: 10), 54.9, accuracy: 0.5)
        
        XCTAssertEqual(model.maxTQ30min(altitude: 1000, fat: 31), 111.4, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 2500, fat: 22), 110.7, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 5200, fat: 15), 102.6, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 6500, fat: 10), 99.2, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 8100, fat: 1), 96.0, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 11000, fat: -4), 86.6, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 12500, fat: -8), 82.5, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 15500, fat: -10), 72.8, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 16900, fat: -15), 69.5, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 19000, fat: -20), 64.3, accuracy: 0.5)
        XCTAssertEqual(model.maxTQ30min(altitude: 19300, fat: -24), 63.5, accuracy: 0.5)
 
    }
    
    func testHoverMetodu() {
        
        XCTAssertEqual(model.hoverIlkBolum(altitude: -1000, fat: -10), 10.0, accuracy: 0.5)
        XCTAssertEqual(model.hoverIlkBolum(altitude: 2000, fat: 20), 278.5, accuracy: 0.5)
        XCTAssertEqual(model.hoverIlkBolum(altitude: 5000, fat: -60), 105.0, accuracy: 0.5)
        XCTAssertEqual(model.hoverIlkBolum(altitude: 19000, fat: 10), 1081.0, accuracy: 0.5)
        
        XCTAssertEqual(model.hoverIkinciBolum(araDeger: 500, gw: 13000), 56.3, accuracy: 0.5)
        XCTAssertEqual(model.hoverIkinciBolum(araDeger: 300, gw: 18500), 85.7, accuracy: 0.5)
        XCTAssertEqual(model.hoverIkinciBolum(araDeger: 580, gw: 17300), 86.5, accuracy: 0.5)
        XCTAssertEqual(model.hoverIkinciBolum(araDeger: 1100, gw: 14800), 97.9, accuracy: 0.5)
        
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 100, hoverHight: 5), 79.0, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 100, hoverHight: 10), 84.8, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 100, hoverHight: 20), 91.9, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 100, hoverHight: 40), 97.2, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 100, hoverHight: 50), 100.0, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 100, hoverHight: 60), 100.0, accuracy: 0.5)
        
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 80, hoverHight: 8), 66.5, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 70, hoverHight: 15), 62.7, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 60, hoverHight: 30), 57.1, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 50, hoverHight: 45), 49.3, accuracy: 0.5)
        XCTAssertEqual(model.hoverUcuncuBolum(araDeger: 55, hoverHight: 55), 55.2, accuracy: 0.5)
        
        XCTAssertEqual(model.hoverIcınGerekliTq(altitude: 3000, fat: 30, gw: 19500, HoverHight: 10), 80.8, accuracy: 0.5)
        XCTAssertEqual(model.hoverIcınGerekliTq(altitude: 2000, fat: 25, gw: 18200, HoverHight: 10), 71.6, accuracy: 0.5)
        XCTAssertEqual(model.hoverIcınGerekliTq(altitude: 2000, fat: 25, gw: 18200, HoverHight: 50), 83.5, accuracy: 0.5)
        XCTAssertEqual(model.hoverIcınGerekliTq(altitude: 5800, fat: 13, gw: 17200, HoverHight: 10), 69.5, accuracy: 0.5)
        XCTAssertEqual(model.hoverIcınGerekliTq(altitude: 5800, fat: 13, gw: 17200, HoverHight: 50), 80.9, accuracy: 0.5)
        
        XCTAssertEqual(model.hoverIcınGerekliTq(altitude: 4000, fat: 10, gw: 21760, HoverHight: 50), 112.2, accuracy: 0.5)
        
    }
    
    func testHoverMaxGw() {
        XCTAssertEqual(Double(model.hoverMaxGw(altitude: 2000, fat: 10, tq: 70.0, hoverHight: 10)), 18162.0, accuracy: 0.5)
        
        
    }
    
    func testSehayatKartlari() {
        
        XCTAssertNotNil(model.seyahatKartiGonderTamDeger(altitude: 0, fat: 10))
        XCTAssertNotNil(model.seyahatKartiGwSeciliTamIrtifaVeFat(altitude: 6000, fat: 20, gw: 19000))
        XCTAssertNil(model.seyahatKartiGonderTamDeger(altitude: 20000, fat: 20))
        XCTAssertNil(model.seyahatKarti(altitude: 14000, fat: 28, gw: 20100))
        
        // vmin
        
        XCTAssertEqual(model.vmin(altitude: 0, fat: 10, gw: 18000, tq: 45.0).0, 52.6, accuracy: 0.5)
        
        XCTAssertEqual(model.vmin(altitude: 0, fat: 25, gw: 18000, tq: 50.0).0, 37.38, accuracy: 0.5)
        
        XCTAssertEqual(model.vmin(altitude: 4000, fat: 25, gw: 18000, tq: 45.0).0, 50.91, accuracy: 0.5)
        
        XCTAssertEqual(model.vmin(altitude: 20000, fat: 30, gw: 18000, tq: 45.0).1!, "No Chart Data")
        
        XCTAssertEqual(model.vmin(altitude: 0, fat: 30, gw: 18000, tq: 15.0).1, "Not Available")
        
        XCTAssertEqual(model.vmin(altitude: 0, fat: 20, gw: 18000, tq: 45.0).0, 52.15, accuracy: 0.5)
        
        XCTAssertEqual(model.vmin(altitude: 1000, fat: 20, gw: 18000, tq: 45.0).0, 51.8, accuracy: 0.5)
      
        XCTAssertEqual(model.vmin(altitude: 2000, fat: 20, gw: 18000, tq: 45.0).0, 51.5, accuracy: 0.5)
        
        
        // vmax
        
        XCTAssertEqual(model.vmax(altitude: 0, fat: 10, gw: 18000, tq: 45.0).0, 91.5, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 0, fat: 20, gw: 18000, tq: 45.0).0, 91.8, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 0, fat: 30, gw: 18000, tq: 45.0).0, 91.65, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 0, fat: 25, gw: 18000, tq: 45.0).0, 91.7, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 4000, fat: 20, gw: 18000, tq: 45.0).0, 88.6, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 4000, fat: 30, gw: 18000, tq: 45.0).0, 86.8, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 4000, fat: 25, gw: 18000, tq: 45.0).0, 87.7, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 20000, fat: 30, gw: 18000, tq: 45.0).1!, "No Chart Data")
        
        XCTAssertEqual(model.vmax(altitude: 0, fat: 30, gw: 18000, tq: 10.0).1, "Not Available")
        
        XCTAssertEqual(model.vmax(altitude: 0, fat: 20, gw: 18000, tq: 50.0).0, 106.7, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 1000, fat: 20, gw: 18000, tq: 50.0).0, 105.8, accuracy: 0.5)
        
        XCTAssertEqual(model.vmax(altitude: 2000, fat: 20, gw: 18000, tq: 50.0).0, 104.8, accuracy: 0.5)
        
        // vtoss
        
        XCTAssertEqual(model.vToss(gw: 14000, maxTq: 45.0, altitude: 2000, fat: 10), "22")
        
        XCTAssertEqual(model.vToss(gw: 14000, maxTq: 45.0, altitude: 0, fat: 10), "23")
        
        XCTAssertEqual(model.vToss(gw: 14000, maxTq: 45.0, altitude: 1000, fat: 10), "22")
        
        XCTAssertEqual(model.vToss(gw: 20000, maxTq: 55.0, altitude: 4000, fat: 0), "46")
        
        XCTAssertEqual(model.vToss(gw: 20000, maxTq: 55.0, altitude: 4000, fat: 10), "46")
        
        XCTAssertEqual(model.vToss(gw: 20000, maxTq: 55.0, altitude: 4000, fat: 5), "46")
        
        XCTAssertEqual(model.vToss(gw: 20000, maxTq: 15.0, altitude: 4000, fat: 10), "Not Available")
        
        // vbroc
        
        XCTAssertEqual(model.vBroc(altitude: 2000, fat: 10, gw: 18000, tq: 45.0), "71 - 3 = 68 kt")
        
        XCTAssertEqual(model.vBroc(altitude: 4000, fat: 10, gw: 18000, tq: 50.0), "69 - 3 = 66 kt")
        
        XCTAssertEqual(model.vBroc(altitude: 2000, fat: 10, gw: 15500, tq: 45.0), "66 - 4 = 62 kt")
        
        XCTAssertEqual(model.vBroc(altitude: 5500, fat: 15, gw: 21000, tq: 51.8), "Not Available")
        
        XCTAssertEqual(model.vBroc(altitude: 18000, fat: -5, gw: 18000, tq: 30.0), "Not Available")
        
        XCTAssertEqual(model.vBroc(altitude: 6000, fat: 0, gw: 17500, tq: 45.0), "67 - 4 = 63 kt")
        
        XCTAssertEqual(model.vBroc(altitude: 7000, fat: -20, gw: 19000, tq: 55.0), "69 - 3 = 66 kt")
        
        XCTAssertEqual(model.vBroc(altitude: 2500, fat: -15, gw: 18000, tq: 60.0), "70 - 3 = 67 kt")
        
        XCTAssertEqual(model.vBroc(altitude: 0, fat: 10, gw: 22000, tq: 12.0), "Not Available")
    
    }
    
    func testClimbKarti() {
        
        // climb kartı
        
        XCTAssertEqual(Double(model.climbKarti(tq: 12.0, gw: 22000)!), Double(465), accuracy: 0.5)
        
        XCTAssertEqual(Double(model.climbKarti(tq: 2.0, gw: 18000)!), Double(112), accuracy: 0.5)
        
        XCTAssertEqual(Double(model.climbKarti(tq: 15.0, gw: 19000)!), Double(678), accuracy: 0.5)
        
        XCTAssertEqual(Double(model.climbKarti(tq: 2.0, gw: 19000)!), Double(106), accuracy: 0.5)
        
        
        
    }
    
    func testMaxRateOfClimb() {
        // maxRateOfClimb tq
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 2000, fat: 10, gw: 20000)!), 47.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 0, fat: -20, gw: 22000)!), 52.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 0, fat: 0, gw: 16650)!), 40.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 0, fat: 40, gw: 22000)!), 52.5, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 0, fat: 40, gw: 20000)!), 47.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 6800, fat: 10, gw: 19500)!), 48.1, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 6000, fat: 10, gw: 19500)!), 47.5, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 8000, fat: 10, gw: 19500)!), 49.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbTQ(altitude: 8000, fat: 10, gw: 23500)!), 72.0, accuracy: 0.5)
        
        // maxRateOfClimb ias
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 0, fat: 40, gw: 22000)!), 76.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 0, fat: 0, gw: 18000)!), 72.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 0, fat: 0, gw: 16850)!), 69.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 0, fat: 40, gw: 20000)!), 73.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 6800, fat: 10, gw: 19500)!), 69.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 6000, fat: 10, gw: 19500)!), 69.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 8000, fat: 10, gw: 19500)!), 68.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbKias(altitude: 8000, fat: 10, gw: 23500)!), 64.0, accuracy: 0.5)
        
        // maxRateOfClimb tq icin gw
        
        XCTAssertNil(model.maxRateOfClimbIcinGW(tq: 24.0, altitude: 0, fat: 55))
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 32.0, altitude: 0, fat: 20)!), 13000.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 32.0, altitude: 2000, fat: 20)!), 13500.0, accuracy: 0.5)
        
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 32.0, altitude: 300, fat: 24)!), 13070.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 56.0, altitude: 2000, fat: 20)!), 22897.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 58.0, altitude: 8000, fat: 20)!), 21325.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 69.0, altitude: 8000, fat: 30)!), 22410.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 69.0, altitude: 10000, fat: 30)!), 21157.0, accuracy: 0.5)
        
        XCTAssertNil(model.maxRateOfClimbIcinGW(tq: 92.0, altitude: 9900, fat: 46))
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 104.0, altitude: 13500, fat: 28)!), 20000.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 42.0, altitude: 14000, fat: 0)!), 16363.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRateOfClimbIcinGW(tq: 110.0, altitude: 18000, fat: -10)!), 18000.0, accuracy: 0.5)

    }
    
    func testSeyahatMaxRocKarti(){
        
        XCTAssertNotNil(model.seyahatMaxRocKartiGonderTamDeger(altitude: 0, fat: 10))
        XCTAssertNotNil(model.seyahatMaxRocKartiGwSeciliTamIrtifaVeFat(altitude: 6000, fat: 20, gw: 19000))
        XCTAssertNil(model.seyahatMaxRocKartiGonderTamDeger(altitude: 20000, fat: 20))
        XCTAssertNil(model.seyahatMaxRocKarti(altitude: 14000, fat: 28, gw: 20100))
        XCTAssertNotNil(model.seyahatMaxRocKarti(altitude: 3000, fat: 13, gw: 19250))
        
        let deger = model.seyahatMaxRocKarti(altitude: 2000, fat: 10, gw: 17500) as! Dictionary<String, Double>
        
        XCTAssertEqual(deger, ["70.0": 41])
        
        
    }
    
    func testMaxRange() {
        
        // max Range TQ
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 0, fat: 0, gw: 20000)!), 76.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 2000, fat: 0, gw: 20000)!), 74.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 4000, fat: -20, gw: 13000)!), 57.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 6000, fat: -10, gw: 17000)!), 63.5, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 7000, fat: -10 , gw: 17000)!), 62.2, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 8000, fat: -10, gw: 17000)!), 61.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 2000, fat: -30, gw: 13000)!), 60.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 14000, fat: -20, gw: 21000)!), 84.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeTq(altitude: 18000, fat: -10, gw: 15000)!), 51.0, accuracy: 0.5)
        
        
        // max Range Kias
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 0, fat: 0, gw: 15000)!), 137.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 18000, fat: 10, gw: 16000)!), 62.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 14000, fat: -20, gw: 17000)!), 95.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 8000, fat: -10, gw: 18000)!), 117.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 6000, fat: 10, gw: 19000)!), 120.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 5000, fat: 10, gw: 19000)!), 124.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 4000, fat: 10, gw: 19000)!), 128.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 2000, fat: -20, gw: 21000)!), 133.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 2000, fat: -10, gw: 21000)!), 132.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.maxRangeKias(altitude: 0, fat: -10, gw: 15500)!), 137.0, accuracy: 0.5)
        
    }
    
    func testSeyahatMaxTq() {
        
        // 2.5 min
        XCTAssertEqual((model.seyahatMaxTq2min(altitude: 4000, fat: -30, gw: 18000, tr: 1.0, antiIce: false, heater: false)!), 59.7, accuracy: 0.5)
        
        XCTAssertNil(model.seyahatMaxTq2min(altitude: 8000, fat: 0, gw: 22000, tr: 1.0, antiIce: false, heater: false))
        
    //    XCTAssertEqual((model.seyahatMaxTq2min(altitude: 0, fat: -10, gw: 22000)!), 67.8, accuracy: 0.5)
        
        // 10 min
        
        XCTAssertEqual((model.seyahatMaxTq10min(altitude: 4000, fat: -30, gw: 22000, tr: 1.0, antiIce: false, heater: false)!), 121.6, accuracy: 0.5)
        
        // 30 min
        
        XCTAssertEqual((model.seyahatMaxTq30min(altitude: 6000, fat: -10, gw: 22000, tr: 1.0, antiIce: false, heater: false)!), 112.6, accuracy: 0.5)
        // mcp
        
        XCTAssertEqual((model.seyahatMaxTqMcp(altitude: 8000, fat: 0, gw: 17000, tr: 1.0, antiIce: false, heater: false)!), 95.3, accuracy: 0.5)
        
        // surate dayalı mcp
        
        XCTAssertEqual((model.SeyahatMcpSurateDayali(altitude: 2000, fat: 10, surat: 120, tr: 1.0, antiIce: false, heater: false)!), 112.8, accuracy: 0.5)
    }

    func testSeyahatMaxTqSurateDayali() {
        
        // 2.5min
        
        XCTAssertEqual((model.seyahatMax2minSurateDayali(altitude: 2000, fat: 10, surat: 100, tr: 1.0, antiice: false, heater: false)!), 64.2, accuracy: 0.5)
        
        XCTAssertEqual((model.seyahatMax2minSurateDayali(altitude: 18000, fat: -10, surat: 75, tr: 1.0, antiice: false, heater: false)!), 35.5, accuracy: 0.5)
        
        // 10 min
        
        XCTAssertEqual((model.seyahatMax10minSurateDayali(altitude: 18000, fat: -10, surat: 75, tr: 1.0, antiice: false, heater: false)!), 70.3, accuracy: 0.5)
        
        // 30 min
        
        XCTAssertEqual((model.seyahatMax30minSurateDayali(altitude: 18000, fat: -10, surat: 75, tr: 1.0, antiice: false, heater: false)!), 66.8, accuracy: 0.5)
        
        // mcp
        
        XCTAssertEqual((model.seyahatMaxMCPSurateDayali(altitude: 18000, fat: -10, surat: 75, tr: 1.0, antiice: false, heater: false)!), 63.2, accuracy: 0.5)
        
    }
    
    func testAirSpeedCorrection() {
        
        XCTAssertEqual(Double(model.airspeedCorrection(ias: 80, varyo: 1000)), Double(-2), accuracy: 0.5)
        
        XCTAssertEqual(Double(model.airspeedCorrection(ias: 70, varyo: 2000)), Double(12), accuracy: 0.5)
        
    }
    
    func testVneKarti() {
    
        XCTAssertEqual(Double(model.vne(fat: 10, altitude: 2000, gw: 18000)), 185.0, accuracy: 0.5)
    
    }
    
    func testBladeStallKarti() {
        
        XCTAssertEqual(Double(model.bladeStall(altitude: 2000, fat: 10, gw: 20000, angle: 30)), 170.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.bladeStall(altitude: 4000, fat: 40, gw: 22000, angle: 30)), 104.0, accuracy: 0.5)
        
    }
    
    func testYakitAkisi() {
        
        XCTAssertEqual(Double(model.yakitAkisiPerEngine(tq: 70, altitude: 0, fat: 20)), 548.0, accuracy: 0.5)
    }
    
    func testTrueAirSpeed() {
        
        XCTAssertEqual(Double(model.iasToTasTamDeger(altitude: 2000, fat: 10, ias: 85)), 90.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.iasToTasTamDeger(altitude: 2000, fat: 10, ias: 0)), 0.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.iasToTas(altitude: 0, fat: 0, ias: 76)), 77.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.iasToTas(altitude: 2000, fat: 0, ias: 76)), 80.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.iasToTas(altitude: 1000, fat: 0, ias: 76)), 78.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.iasToTas(altitude: 1000, fat: 10, ias: 76)), 80.0, accuracy: 0.5)
        
        XCTAssertEqual(Double(model.iasToTas(altitude: 1000, fat: 5, ias: 76)), 79.0, accuracy: 0.5)
        
        

        
        
        
        
        
    }
    
}

























