//
//  Conditions.swift
//  S70PPC
//
//  Created by Mustafa Karpat on 26.01.2017.
//  Copyright © 2017 Karpat. All rights reserved.
//

import Foundation

struct TakeoffConditions {
    
    var altitude: Int?        // preassure altitude   0 - 20.000 arası olabilir.
    
    var fat : Int?            // free Air Temperature -60 / 60 arası olabilir.
    
    var antiIce = false
    
    var heater = false
    
    var aircraft: Aircraft?
    
    var gw : GrossWeight?
    
    var gwOnerisi:String? = nil
    
    func maxGwOnerilebilir() -> Bool{
        
        if altitude != nil && fat != nil && aircraft?.etf1 != nil && aircraft?.etf2 != nil {
            return true
            }else {
            return false
        }
        
    }
    
    mutating func setAltitude(value: Int) -> (kabul: Bool, alertBasligi:String, alertMetni: String ) {
        
        if value < 0 || value > 20000 {
             // hata mesajı lazım.
            
            return(false, "Invalid Entry", "Pressure altitude must be between 0 - 20.000 feets")
            
        } else {
            
            altitude = value
            return(true, "","")
        }
        
    }
    
    mutating func setFat(value: Int) -> (kabul: Bool, alertBasligi:String, alertMetni: String ) {
   
        if value < -54 || value > 60 {
    // hata mesajı lazım.
    
    return(false, "Invalid Entry", "Free Air Temperature must be between -54 - 60 ℃")
    
    } else {
    
    fat = value
    return(true, "","")
    }

    }
    
    func validate() -> (kabulEdildi: Bool, baslik: String, mesaj: String) {
        
        if altitude != nil && fat != nil && aircraft != nil && gw != nil {
            
            // aircraft ve gw deger var ama içeriği uygun olmayabilir. 
            
            if (aircraft?.validate().kabulEdildi)! {
                
                if (gw?.validate().kabulEdildi)! {
                    return (true,"","")
                } else {
                    return (gw?.validate())!
                }
    
                
            } else {
                return (aircraft?.validate())!
            }
            
            
        } else {
            
           
            var message = ""
            
            if altitude == nil {
                message = message.appending("Pressure Altitude, ")
            }
            if fat == nil {
                message = message.appending("Free Air Temperature, ")
            }
            
            if aircraft == nil {
                
                message = message.appending("Etf1, Etf2, ")
                
            } else {
                
                let deger = aircraft?.validate()
                
                if deger?.kabulEdildi == false {
                    
                    message = message.appending((deger?.mesaj)!)
                    
                }
                
            }
            
            
            
            if gw == nil {
                message = message.appending("Gross Weight")
            }
            
            return (false, "Missing Entry", message)
            
        }
    }
}

struct CruiseConditions {
    
    var altitude: Int?         // seyahat irtifası
    
    var fat: Int?              // seyahat sicakliği

    var cruiseSpeed: Int?            //seyahattte kulanılacak sürat. max roc ile max range arası olması tavsiye edilir.
    
    var antiIce = false
    
    var heater = false
    
    var aircraft: Aircraft?
    
    var gw : GrossWeight?
    
    var tavsiyeEdilenSeyahatSurati: Int?
    
    mutating func setAltitude(value: Int) -> (kabul: Bool, alertBasligi:String, alertMetni: String ) {
        
        if value < 0 || value > 20000 {
            // hata mesajı lazım.
            
            return(false, "Invalid Entry", "Pressure altitude must be between 0 - 20.000 feets")
            
        } else {
            
            altitude = value
            return(true, "","")
        }
        
    }
    
    mutating func setFat(value: Int) -> (kabul: Bool, alertBasligi:String, alertMetni: String ) {
        
        if value < -60 || value > 60 {
            // hata mesajı lazım.
            
            return(false, "Invalid Entry", "Free Air Temperature must be between -60 - 60 ℃")
            
        } else {
            
            fat = value
            return(true, "","")
        }
        
    }
    
    mutating func setCruiseSpeed(value: Int) -> (kabul: Bool, alertBasligi:String, alertMetni: String ) {
      
        cruiseSpeed = value
        return(true, "","")
        
        
//        if altitude != nil {
//            if fat != nil {
//                if aircraft?.etf1 != nil && aircraft?.etf2 != nil   {
//                    if gw?.totalGW != nil {
//
//                        let model = S70PPC()
//
//                        var vh = model.vhSuratiOner(etf1: aircraft!.etf1! , etf2: aircraft!.etf2!, altitude: altitude!, fat: fat!, surat: value, gw: (gw?.totalGW)!)!
//
//                        var vneKart = model.vne(fat: fat!, altitude: altitude!, gw: (gw?.totalGW)!)
//
//                        let vneBladeStall = model.bladeStall(altitude: altitude!, fat: fat!, gw: (gw?.totalGW)!, angle: 0)
//
//                        if vneKart > vneBladeStall {
//                            vneKart = vneBladeStall
//                        }
//
//                        if vh > vneKart {
//                            vh = vneKart
//                        }
//
//                        let maxRoc = model.maxRocSuratiOner(altitude: altitude!, fat: fat!, gw: (gw?.totalGW)!)
//
//                        if value <= vh && value >= maxRoc! {
//
//                            cruiseSpeed = value
//                            return (true,"","")
//
//                        } else {
//
//                            return (false, "Invalid Entry", "Cruise Speed should be between MaxRoC (\(maxRoc!) kt) and Vh (\(vh) kt) speeds.")
//                        }
//
//                    } else {
//                         return (false, "Missing Entry", "Please enter Gross Weight first.")
//                    }
//
//                } else {
//                    return (false, "Missing Entry", "Please enter ETF values first.")
//                }
//            } else {
//                return (false, "Missing Entry", "Please enter an Air Temperaure first.")
//
//            }
//        } else {
//            return (false, "Missing Entry", "Please enter an Altitude first.")
//        }
//
//
    }
    
    
    
    func seyahatSuratiOner(seyahatAltitude: Int, SeyahatFat: Int, seyahatTotalGw: Int) -> Int{
        return 0 // düzenlenecek.
    }
    
    func vhSuratiHesapla(etf1: Double, etf2: Double, seyahatFat: Int, seyahatSurati: Int, seyahatAltitude: Int, seyahatTotalGw: Int, seyahatAntiice : Bool, seyahatHeater: Bool) -> Int {
        return 0
    }
    
    func maxRocSuratiHesapla(seyahatAltitude: Int, sehayatFat: Int, seyahatTotalGw: Int) -> Int {
        return 0
    }

    mutating func cruiseSuratOnerisiMevcut() -> Bool {
        
        if altitude != nil {
            if fat != nil {
                if gw?.totalGW != nil {
                    if aircraft?.etf1 != nil && aircraft?.etf2 != nil {
                        let model = S70PPC()
                        
                        if let oneri = model.seyahatSuratiOner(altitude: altitude!, fat: fat!, gw: (gw?.totalGW)!) {
                            
                            tavsiyeEdilenSeyahatSurati = Int(oneri)
                            return true
                            
                        } else {
                            return false
                        }
                    } else {
                        return false
                    }
                    

                } else {
                   return false
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }
    func validate() -> (kabulEdildi: Bool, baslik: String, mesaj: String) {
        
        if altitude != nil && fat != nil && aircraft != nil && gw != nil && cruiseSpeed != nil {
            
            // aircraft ve gw deger var ama içeriği uygun olmayabilir.
            
            if (aircraft?.validate().kabulEdildi)! {
                
                if (gw?.validate().kabulEdildi)! {
                    return (true,"","")
                } else {
                    return (gw?.validate())!
                }
                
                
            } else {
                return (aircraft?.validate())!
            }
            
            
        } else {
            
            
            var message = ""
            
            if altitude == nil {
                message = message.appending("Pressure Altitude, ")
            }
            if fat == nil {
                message = message.appending("Free Air Temperature, ")
            }
            
            if cruiseSpeed == nil {
                message = message.appending("Cruise Speed, ")
            }
            
            if aircraft == nil {
                
                message = message.appending("Etf1, Etf2, ")
                
            } else {
                
                let deger = aircraft?.validate()
                
                if deger?.kabulEdildi == false {
                    
                    message = message.appending((deger?.mesaj)!)
                    
                }
                
            }
            
            
            
            if gw == nil {
                message = message.appending("Gross Weight")
            }
            
            return (false, "Missing Entry", message)
            
        }
    }

    
    
}

struct LandingConditions {
    
    var altitude: Int?        // varış koşullarındaki irtifa.
    
    var fat: Int?             // variş koşulları sicakliği
    
    var antiIce = false
    
    var heater = false
    
    var aircraft: Aircraft?
    
    var gw : GrossWeight?
    
    mutating func setAltitude(value: Int) -> (kabul: Bool, alertBasligi:String, alertMetni: String ) {
        
        if value < 0 || value > 20000 {
            // hata mesajı lazım.
            
            return(false, "Invalid Entry", "Pressure altitude must be between 0 - 20.000 feets")
            
        } else {
            
            altitude = value
            return(true, "","")
        }
        
    }
    
    mutating func setFat(value: Int) -> (kabul: Bool, alertBasligi:String, alertMetni: String ) {
        
        if value < -54 || value > 60 {
            // hata mesajı lazım.
            
            return(false, "Invalid Entry", "Free Air Temperature must be between -54 - 60 ℃")
            
        } else {
            
            fat = value
            return(true, "","")
        }
        
    }
    
    func validate() -> (kabulEdildi: Bool, baslik: String, mesaj: String) {
        
        if altitude != nil && fat != nil && aircraft != nil && gw != nil {
            
            // aircraft ve gw deger var ama içeriği uygun olmayabilir.
            
            if (aircraft?.validate().kabulEdildi)! {
                
                if (gw?.validate().kabulEdildi)! {
                    return (true,"","")
                } else {
                    return (gw?.validate())!
                }
                
                
            } else {
                return (aircraft?.validate())!
            }
            
            
        } else {
            
            
            var message = ""
            
            if altitude == nil {
                message = message.appending("Pressure Altitude, ")
            }
            if fat == nil {
                message = message.appending("Free Air Temperature, ")
            }
            
            if aircraft == nil {
                
                message = message.appending("Etf1, Etf2, ")
                
            } else {
                
                let deger = aircraft?.validate()
                
                if deger?.kabulEdildi == false {
                    
                    message = message.appending((deger?.mesaj)!)
                    
                }
                
            }
            
            
            
            if gw == nil {
                message = message.appending("Gross Weight")
            }
            
            return (false, "Missing Entry", message)
            
        }
    }
}

















